# BTC Over 9000

This is a simple Twitter bot that watches the Bitcoin price on BitMEX and tweets a Vegeta GIF every time it crosses over 9000.

## How to use

1. You will need credentials for the Twitter API, once you have them, copy the `.env.example` file to `.env` and fill it with you credentials.
2. Install the dependencies with `npm install` or `yarn install`.
3. Run `node index.js` and... wait.

## Sending a test tweet

You can send a tweet to test that your credentials are working, by configuring them as mentioned above and then running `node testTweet.js`.
