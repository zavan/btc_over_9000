const path = require('path')
const Twit = require('twit')

const twit = new Twit({
  consumer_key: process.env.CONSUMER_KEY,
  consumer_secret: process.env.CONSUMER_SECRET,
  access_token: process.env.ACCESS_TOKEN_KEY,
  access_token_secret: process.env.ACCESS_TOKEN_SECRET
})

const uploadMedia = (fileName) => new Promise((resolve, reject) => {
  const mediaPath = path.join(__dirname, 'gifs', fileName)

  twit.postMediaChunked({ file_path: mediaPath }, (err, data, response) => {
    if (err) return reject(err)

    setTimeout(() => {
      resolve(data.media_id_string)
    }, data.processing_info.check_after_secs * 1500 || 1500)
  })
})

const postTweet = (data) => new Promise((resolve, reject) => {
  twit.post('statuses/update', data, (err, data, response) => {
    if (err) return reject(err)
    resolve(data)
  })
})

module.exports = { uploadMedia, postTweet }
