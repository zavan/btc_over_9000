require('dotenv').config()

const BitMEXClient = require('bitmex-realtime-api')
const { uploadMedia, postTweet } = require('./twitter')

const mex = new BitMEXClient({ testnet: false })

let previousPrice = 10000
let lastTweet = 0

mex.addStream('XBTUSD', 'trade', (data, _symbol, _tableName) => {
  if (!data.length) return
  const lastPrice = data[data.length - 1].price

  const now = Math.floor(Date.now() / 1000)
  const secondsSinceLastTweet = now - lastTweet

  if (process.env.DEBUG) {
    console.debug(previousPrice, lastPrice, secondsSinceLastTweet)
  }

  if (lastPrice > 9000 && previousPrice <= 9000 && secondsSinceLastTweet > 1800) {
    lastTweet = Math.floor(Date.now() / 1000)

    console.log("IT'S OVER 9000!!!")

    uploadMedia('vegeta.gif').then((mediaId) => (
      postTweet({
        status: "IT'S OVER 9000!!! 🤖",
        media_ids: mediaId
      })
    )).then(console.log).catch(console.error)
  }

  previousPrice = lastPrice
})
