require('dotenv').config()

const { uploadMedia, postTweet } = require('./twitter')

uploadMedia('goku.gif').then((mediaId) => (
  postTweet({
    status: 'This tweet is just a test 🤖',
    media_ids: mediaId
  })
)).then(console.log).catch(console.error)
